from datetime import datetime
import pandas as pd
from random import random

from GA import GA
from poi import POI
from utils import select_best_from_population, show_route, get_best_parent, make_crossover

PATH = 'data.csv'

keys = ['M', 'D', 'P_SIZE', 'T_SIZE', 'N', 'iterations', 'fitness',  'distance', 'value', 'prepare_time', 'time', 'total_time']

input_data = [
    {'M': 120, 'D': 0, 'P_SIZE': 300, 'T_SIZE': 5, 'N': 100},
    {'M': 150, 'D': 0, 'P_SIZE': 300, 'T_SIZE': 5, 'N': 100},
    {'M': 170, 'D': 0, 'P_SIZE': 300, 'T_SIZE': 5, 'N': 100},
    {'M': 210, 'D': 0, 'P_SIZE': 300, 'T_SIZE': 5, 'N': 100},
    {'M': 420, 'D': 0, 'P_SIZE': 300, 'T_SIZE': 5, 'N': 100},
    {'M': 120, 'D': 1, 'P_SIZE': 300, 'T_SIZE': 5, 'N': 100},
    {'M': 150, 'D': 1, 'P_SIZE': 300, 'T_SIZE': 5, 'N': 100},
    {'M': 170, 'D': 1, 'P_SIZE': 300, 'T_SIZE': 5, 'N': 100},
    {'M': 210, 'D': 1, 'P_SIZE': 300, 'T_SIZE': 5, 'N': 100},
    {'M': 420, 'D': 1, 'P_SIZE': 300, 'T_SIZE': 5, 'N': 100}
]


def make_pois_from_df(df):
    chargin_stations = []
    pois = []

    chargers_index = 1
    pois_index = 1

    for _, item in df.iterrows():
        item = POI(
            pk=chargers_index if item.value == 0.0 else pois_index,
            place=item.place,
            lat=item.lat,
            lon=item.lon,
            value=item.value,
            poi_type='charger' if item.value == 0.0 else 'attraction'
        )

        if item.value == 0.0:
            chargin_stations.append(item)
            chargers_index += 1
        else:
            pois.append(item)
            pois_index += 1

    return pois, chargin_stations


pois, chargers = make_pois_from_df(pd.read_csv(PATH))


def show_results(keys, data):
    for key in keys:
        if key in data:
            print('{}: {}'.format(key, data[key]))
    print('\nn---------------------------------------------------------------\n')


def perform_algorithm(M, D, P_SIZE, T_SIZE, N):
    time = None

    start_index = 0
    end_index = 2

    population = [GA(PATH, start_index, end_index, M, D, pois=list(pois), chargers=list(chargers)) for _ in range(P_SIZE)]

    best_values = [get_best_parent(population).value]

    prepare_time = None
    for item in population:
        if not prepare_time:
            prepare_time = item.prepare_init_route_time
        else:
            prepare_time += item.prepare_init_route_time

    print('--- creating initial data processed in: {} ---'.format(prepare_time))

    for i in range(N):
        now = datetime.now()
        population = select_best_from_population(population, T_SIZE)  # , M, D, pois, chargin_stations)

        for _ in range(10):
            population = make_crossover(population, M, D)

        for parent in population:
            parent.make_mutation()
            parent.remove_lowest_fitness_poi()

        if not time:
            time = datetime.now() - now
        else:
            time += datetime.now() - now

        best_values.append(get_best_parent(population).value)

        if len(best_values) > 3 and best_values[-1] == best_values[-2] and best_values[-2] == best_values[-3]:
            break

    best_candidate = get_best_parent(population)

    print('--- total time: {} ---'.format(time))

    data = {
        'M': M,
        'D': D,
        'P_SIZE': P_SIZE,
        'T_SIZE': T_SIZE,
        'N': N,
        'iterations': len(best_values),
        'fitness': best_candidate.fitness(),
        'distance': best_candidate.get_route_distance(),
        'value': best_candidate.value,
        'prepare_time': prepare_time,
        'time': time,
        'total_time': prepare_time + time
    }

    return {
        'data': data,
        'route': best_candidate.route,
        'best_values': best_values
    }


single = False
# single = True


if __name__ == '__main__':
    if single:
        M = 150  # max tour length
        D = 0  # loads amount
        P_SIZE = 300  # population size (check 300)
        T_SIZE = 5  # number of individuals
        N = 100  # GA generations

        result = perform_algorithm(M, D, P_SIZE, T_SIZE, N)
        show_results(keys, result['data'])
        print(result['best_values'])
        df = pd.DataFrame([result['data']], columns=keys)
        print(df)
    else:
        results = []
        for input in input_data:
            options = [perform_algorithm(**input) for _ in range(10)]
            # print('TEST ', [item['data']['value'] for item in options])
            best_val = 0
            result = None

            for item in options:
                if item['data']['value'] > best_val:
                    best_val = item['data']['value']
                    result = item

            show_results(keys, result['data'])
            print(result['best_values'], '\n')

            results.append(result['data'])

        df = pd.DataFrame(results, columns=keys)
        df.to_csv('results/results_without_reference.csv')

        print('-- processing complete, results saved to the file --')
