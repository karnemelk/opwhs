import pandas as pd
from math import sqrt, sin, cos, atan2, pi
import random

from poi import POI


class GA:
    def __init__(self, path, start_index, end_index, m, d, create_raw=False, names=None, route=None, pois=None, chargers=None):
        self.m = m
        self.d = d

        self.route = []
        self.tours_distances = []
        self.value = 0

        self.distance = 0

        df = pd.read_csv(path)

        if create_raw:
            self.all_attractions, self.all_stations = self.make_pois_on_ready(df, names)
            self.route = route

            for tour in route:
                self.tours_distances.append(self.get_tour_distance(tour))

            self.value = sum(sum(item.value for item in tour) for tour in self.route)
        else:
            self.start_index = start_index
            self.end_index = end_index

            if pois and chargers:
                self.all_attractions, self.all_stations = pois, chargers
            else:
                self.all_attractions, self.all_stations = self.make_pois_from_df(df)

            self.first_charger = self.all_stations[start_index]
            self.last_charger = self.all_stations[end_index]

            del self.all_stations[start_index]
            del self.all_stations[end_index]
            self.last_charger_index = None

            from datetime import datetime
            now = datetime.now()
            for i in range(d + 1):
                self.route.append(self.create_single_tour(i))
            self.prepare_init_route_time = datetime.now() - now

    def make_pois_on_ready(self, df, names):
        chargin_stations = []
        pois = []

        chargers_index = 1
        pois_index = 1

        for _, item in df.iterrows():
            item = POI(
                pk=chargers_index if item.value == 0.0 else pois_index,
                place=item.place,
                lat=item.lat,
                lon=item.lon,
                value=item.value,
                poi_type='charger' if item.value == 0.0 else 'attraction'
            )

            if item.value == 0.0:
                if item.place not in names:
                    chargin_stations.append(item)
                chargers_index += 1
            else:
                if item.place not in names:
                    pois.append(item)
                pois_index += 1

        return pois, chargin_stations

    def make_pois_from_df(self, df):
        chargin_stations = []
        pois = []

        chargers_index = 1
        pois_index = 1

        for _, item in df.iterrows():
            item = POI(
                pk=chargers_index if item.value == 0.0 else pois_index,
                place=item.place,
                lat=item.lat,
                lon=item.lon,
                value=item.value,
                poi_type='charger' if item.value == 0.0 else 'attraction'
            )

            if item.value == 0.0:
                chargin_stations.append(item)
                chargers_index += 1
            else:
                pois.append(item)
                pois_index += 1

        return pois, chargin_stations

    def create_single_tour(self, i):
        is_last = i == self.d

        tour = [self.first_charger, ]

        total = 0
        selected_charger = None

        while not selected_charger:
            while len(self.all_attractions) > 0:
                index_attr = random.randint(0, len(self.all_attractions) - 1)
                attraction = self.all_attractions[index_attr]

                if is_last:
                    index_charge = None
                    charger = self.last_charger
                else:
                    index_charge = self.end_index if is_last else self.get_nearest_charger(attraction, self.all_stations, index=True)
                    charger = self.all_stations[index_charge]

                if total + self.get_distance(tour[-1], attraction) + self.get_distance(attraction, charger) < self.m:
                    total += self.get_distance(tour[-1], attraction)
                    selected_charger = charger
                    tour.append(attraction)
                    self.value += attraction.value

                    del self.all_attractions[index_attr]

                    self.last_charger_index = index_charge

                    tour = self.perform_for_section(tour, len(tour) - 1)
                else:
                    break

        tour.append(selected_charger)

        self.first_charger = selected_charger

        if not is_last:
            del self.all_stations[self.last_charger_index]

        tour = self.perform_for_section(tour, len(tour) - 1)

        self.tours_distances.append(self.get_tour_distance(tour))

        return tour

    # finds nearest charger point for given point
    def get_nearest_charger(self, attraction, stations, index=False):
        distance = 1e9
        nearest = None
        idx = None

        for i in range(len(stations)):
            station = stations[i]
            temp_dist = self.get_distance(attraction, station)

            if temp_dist < distance:
                distance = temp_dist
                nearest = station
                idx = i

        if index:
            return idx

        return nearest

    # calculate route distance and value
    def get_route_distance(self):
        return sum([self.get_tour_distance(tour) for tour in self.route])

    def get_tour_distance(self, tour):
        return sum([self.get_distance(tour[i - 1], tour[i]) for i in range(1, len(tour))])

    def get_route_value(self):
        # return sum([self.get_tour_value(tour) for tour in self.route])
        return self.value

    # def get_tour_value(self, tour):
    #     return sum([point.value for point in tour])

    # calculate whole route fitness value
    def fitness(self):  # OPTIMIZE!!!!
        # total = self.get_route_value()
        # distance = self.get_route_distance()
        return self.value ** 2 / sum(self.tours_distances)

    # get straight line distance between two points on map
    def get_distance(self, point_a, point_b):
        lat1 = point_a.lat * pi / 180
        lon1 = point_a.lon * pi / 180
        lat2 = point_b.lat * pi / 180
        lon2 = point_b.lon * pi / 180

        dlat = lat2 - lat1
        dlon = lon2 - lon1

        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * (sin(dlon / 2) ** 2)
        c = 2 * atan2(sqrt(a), sqrt(1 - a))
        distance = 6371 * c

        return distance

    def perform_2opt(self):
        for i in range(len(self.route)):
            tour = self.route[i]

            if len(tour) < 4:
                continue

            for j in range(3, len(tour)):
                tour = self.perform_for_section(tour, j)

            self.route[i] = tour

    def perform_for_section(self, tour, index):
        if len(tour) < 4:
            return tour

        last1 = index - 1
        last2 = index

        for i in range(index - 2):
            first1 = i
            first2 = i + 1

            dist_old_1 = self.get_distance(tour[first1], tour[first2])
            dist_old_2 = self.get_distance(tour[last1], tour[last2])

            dist_new_1 = self.get_distance(tour[first1], tour[last1])
            dist_new_2 = self.get_distance(tour[first2], tour[last2])

            if dist_old_1 + dist_old_2 > dist_new_1 + dist_new_2:
                i_left = int(first2)
                i_right = int(last1)
                while i_left < i_right:
                    temp = tour[i_left]
                    tour[i_left] = tour[i_right]
                    tour[i_right] = temp

                    i_left += 1
                    i_right -= 1

                break

        return tour

    def make_mutation(self):
        attraction_index = random.randint(0, len(self.all_attractions) - 1)
        candidate = self.all_attractions[attraction_index]

        best_tour_index = None
        best_position_index = None
        best_candidate_fitness = 0

        for i in range(len(self.route)):
            tour = self.route[i]
            distance = self.tours_distances[i]

            for j in range(1, len(tour)):
                distance_given = self.get_distance(tour[j - 1], candidate) + self.get_distance(candidate, tour[j])
                distance_removed = self.get_distance(tour[j - 1], tour[j])

                if distance - distance_removed + distance_given <= self.m:
                    try:
                        value = (candidate.value ** 2) / distance_given
                        if value > best_candidate_fitness:
                            best_candidate_fitness = value
                            best_tour_index = i
                            best_position_index = j
                    except ZeroDivisionError:
                        continue

        if best_position_index:
            self.push_new_poi(attraction_index, best_tour_index, best_position_index)
            self.perform_2opt()
            self.tours_distances[best_tour_index] = self.get_tour_distance(self.route[best_tour_index])

    def remove_lowest_fitness_poi(self):
        lowest_tour_index = None
        lowest_position_index = None
        lowest_candidate_fitness = 1e9

        for i in range(len(self.route)):
            tour = self.route[i]

            for j in range(1, len(tour) - 1):
                distance_left = self.get_distance(tour[j - 1], tour[j])
                distance_right = self.get_distance(tour[j], tour[j + 1])
                value = tour[j].value
                dist = distance_left + distance_right or 1
                fitness = value ** 2 / dist

                if fitness < lowest_candidate_fitness:
                    lowest_candidate_fitness = fitness
                    lowest_tour_index = i
                    lowest_position_index = j

        if lowest_position_index:
            self.find_best_poi_for_place(lowest_tour_index, lowest_position_index)

    def push_new_poi(self, attraction_index, best_tour_index, best_position_index):
        poi = self.all_attractions.pop(attraction_index)
        self.value += poi.value
        self.route[best_tour_index].insert(best_position_index, poi)

    def find_best_poi_for_place(self, tour_index, position_index):
        best_position_index = None
        best_candidate_fitness = 0

        tour = self.route[tour_index]
        # distance = self.get_tour_distance(tour) TODO????
        distance = self.tours_distances[tour_index]

        left_prev = self.get_distance(tour[position_index - 1], tour[position_index])
        right_prev = self.get_distance(tour[position_index], tour[position_index + 1])
        fitness_prev = tour[position_index].value ** 2 / (left_prev + right_prev)

        best_left = None
        best_right = None

        for i in range(len(self.all_attractions)):
            attraction = self.all_attractions[i]

            left = self.get_distance(tour[position_index - 1], attraction)
            right = self.get_distance(attraction, tour[position_index + 1])
            value = attraction.value
            fitness = value ** 2 / (left + right)

            if fitness > best_candidate_fitness and distance - (left_prev + right_prev) + left + right <= self.m:
                best_candidate_fitness = fitness
                best_position_index = i
                best_left = left
                best_right = right

        if best_candidate_fitness > fitness_prev:
            lowest_poi = self.route[tour_index].pop(position_index)
            best_poi = self.all_attractions[best_position_index]

            self.value = self.value - lowest_poi.value + best_poi.value

            self.route[tour_index].insert(position_index, self.all_attractions[best_position_index])
            self.all_attractions[best_position_index] = lowest_poi

            self.tours_distances[tour_index] = distance - (left_prev + right_prev) + best_left + best_right

