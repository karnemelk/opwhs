import random
from math import sqrt, sin, cos, atan2, pi
from copy import copy


# creates new population with selected routes with best fitness value
from GA import GA


def select_best_from_population(population, k):  # , m, d, pois, chargin_stations):
    new_population = []
    for _ in range(len(population)):
        random_k = select_random_k(population, k)
        best = select_best_from_k(random_k)

        # new_population.append(best)
        new_population.append(copy(best))

    return new_population


# selects route with best fitness value among parents
def select_best_from_k(parents):
    best_fitness = 0
    best_route = None

    for parent in parents:
        fitness = parent.fitness()
        if fitness > best_fitness:
            best_fitness = fitness
            best_route = parent

    return best_route


# selects k random routes from population
def select_random_k(population, k):
    return [random.choice(population) for _ in range(k)]


def get_best_parent(parents):
    best_parent = None
    best_fitness = 0
    for parent in parents:
        fitness = parent.fitness()
        if fitness > best_fitness:
            best_fitness = fitness
            best_parent = parent

    return best_parent


def show_route(parent):
    for tour in parent.route:
        print('-----------------------------------------------------------------------------')
        for place in tour:
            print(str(place) if place.value == 0.0 else '\t{}'.format(str(place)))
        print('-----------------------------------------------------------------------------')
    print('Fitness: {}'.format(parent.fitness()))
    print('Distance: {}'.format(parent.get_route_distance()))
    # print('Value: {}'.format(parent.get_route_value()))
    print('Value: {}'.format(parent.value))


def make_crossover(population, m, d):
    indexA = random.randint(0, len(population) - 1)
    indexB = random.randint(0, len(population) - 1)
    pA = population[indexA]
    pB = population[indexB]

    if pA.value < pB.value:
        lowest_index = indexA
        lowest_value = pA.value
    else:
        lowest_index = indexB
        lowest_value = pB.value

    new_pA, new_pB = get_random_crossover(pA, pB, m, d)

    best_new = None

    if new_pA:
        best_new = new_pA

    if new_pB and best_new is None:
        best_new = new_pB
    elif new_pB and best_new and new_pB.value > best_new.value:
        best_new = new_pB

    if best_new and best_new.value > lowest_value:
        population[lowest_index] = best_new

    return population


def get_random_crossover(pA, pB, m, d):
    pA_route = [pA.route[0][0]]
    pB_route = [pB.route[0][0]]

    for tour in pA.route:
        pA_route.extend(tour[1:])

    for tour in pB.route:
        pB_route.extend(tour[1:])

    pk_list_A = [item.place for item in pA_route[1: -1]]
    pk_list_B = [item.place for item in pB_route[1: -1]]

    inter = list(set(pk_list_A).intersection(pk_list_B)) or None

    if not inter:
        return None, None

    random_poi = random.choice(inter)

    pA_index = pk_list_A.index(random_poi) + 1
    pB_index = pk_list_B.index(random_poi) + 1

    pA_left = pA_route[: pA_index]
    pA_right = pA_route[pA_index:]
    pB_left = pB_route[: pB_index]
    pB_right = pB_route[pB_index:]

    new_pA = pA_left + pB_right
    new_pB = pB_left + pA_right

    output = [None, None]

    if validate_new_route(new_pA, pA_route, pB_route, m, d):
        new_pA_route = []
        temp_tour = None
        new_pA_names = []

        for item in new_pA:
            new_pA_names.append(item.place)

            if item.poi_type == 'charger':
                if not temp_tour:
                    temp_tour = [item]
                else:
                    temp_tour.append(item)
                    new_pA_route.append(temp_tour)
                    temp_tour = [item]
            else:
                temp_tour.append(item)

        new_pA = GA('data.csv', 0, 0, m, d, create_raw=True, names=new_pA_names, route=new_pA_route)

        output[0] = new_pA
    if validate_new_route(new_pB, pA_route, pB_route, m, d):
        new_pB_route = []
        temp_tour = None
        new_pB_names = []

        for item in new_pB:
            new_pB_names.append(item.place)

            if item.poi_type == 'charger':
                if not temp_tour:
                    temp_tour = [item]
                else:
                    temp_tour.append(item)
                    new_pB_route.append(temp_tour)
                    temp_tour = [item]
            else:
                temp_tour.append(item)

        new_pB = GA('data.csv', 0, 0, m, d, create_raw=True, names=new_pB_names, route=new_pB_route)

        output[1] = new_pB

    return output


def get_distance(point_a, point_b):
    lat1 = point_a.lat * pi / 180
    lon1 = point_a.lon * pi / 180
    lat2 = point_b.lat * pi / 180
    lon2 = point_b.lon * pi / 180

    dlat = lat2 - lat1
    dlon = lon2 - lon1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * (sin(dlon / 2) ** 2)
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = 6371 * c

    return distance


def validate_new_route(route, old_pA, old_pB, m, d):
    actual_distance = 0
    total_chargers = 1

    for i in range(1, len(route)):

        actual_distance += get_distance(route[i - 1], route[i])

        if actual_distance > m:
            return False

        if route[i].poi_type == 'charger':
            total_chargers += 1
            actual_distance = 0

    if total_chargers != d + 2:
        return False

    return if_routes_different(route, old_pA) and if_routes_different(route, old_pB)


def if_routes_different(new_route, old_route):
    if len(new_route) != len(old_route):
        return True
    else:
        for itemA, itemB in zip(new_route, old_route):
            if itemA.place != itemB.place:
                return True

    return False


def get_id_list_by_random(population, p_k):
    id_list = list(range(len(population)))
    total_max = len(population) * p_k
    if total_max % 2 != 0:
        total_max = max(0, total_max - 1)
