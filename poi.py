class POI:
    def __init__(self, pk, place, lat, lon, value, poi_type):
        self.pk = pk
        self.place = place
        self.lat = lat
        self.lon = lon
        self.value = value
        self.poi_type = poi_type

    def __str__(self):
        return '#{}. {} at ({}, {}) = {}'.format(self.pk, self.place, self.lat, self.lon, self.value)
        # return '#{}. {} = {}'.format(self.pk, self.place, self.value)
