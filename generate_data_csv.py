import pandas as pd


def generate_csv(data):
    return pd.DataFrame(data)


def parse_lines(data):
    data = [
        {
            'place': ' '.join(item[:-3]),
            'lat': float(item[-3].replace(',', '.')),
            'lon': float(item[-2].replace(',', '.')),
            'value': float(item[-1].replace(',', '.'))
        } for item in data
    ]

    return data


def read_file(path):
    with open(path, encoding="utf8") as f:
        content = f.readlines()
        content = [x.strip().split(' ') for x in content]
        return content


if __name__ == '__main__':
    path = 'data.txt'
    content = read_file(path)
    content = parse_lines(content)
    df = generate_csv(content)
    df.to_csv('data.csv', index=False)
